Description
-----------
This module provides integration between cURL and the Webform module and POSTs the values of a Webform to a URL via cURL.

Requirements
------------
Drupal 7.x
Webform
cURL must be installed on the web server.

Basic Instructions
------------------
1. After enabling the module open a Webform that you which to enable cURL posting on.
2. Click on the Webform tab (or go to /node/[nid]/webform).
3. Click the cURL Settings button from the options (node/[nid]/webform/curl).
4. Click the Enabled radio button and enter the URL you want to POST to.
5. (Optional) You may need to enter a username and password, click the Authentication link and options for the username and password will open.
Note that the password is both stored in plain text and is visible on the page.
6. Submit the form to make sure everything is working.

Advanced Instructions
---------------------
Weborm Curl Integration makes a hook available so that third party modules can alter the data before submission. To do this:

1. Create a new module (at the very least this means creating a YOURMODULE.info and YOURMODULE.module file).
2. Implement hook_webform_curl_submission_alter, this means creating a function called YOURMODULE_webform_curl_submission_alter, below is an example:

function YOURMODULE_webform_curl_submission_alter($node, $submission, &$fields)
{
	$fields['time'] = $timestamp;
	$fields['checksum'] = YOURMODULE_calculate_checksum($fields);
}