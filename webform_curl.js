jQuery(function( $ )
{
	$(document).ready(function()
	{
		if($("#edit-enable-0").attr("checked"))
		{
			$("#edit-webform-curl-settings").hide();
		}

		$("#edit-enable-0").click(function()
		{
			$("#edit-webform-curl-settings").hide();
		});

		$("#edit-enable-1").click(function()
		{
			$("#edit-webform-curl-settings").show();
		});

	});
});